package lesson4;

public class Pair {
    public String symbol;
    public Float freq;

    public Pair(String symbol, Float freq) {
        this.symbol = symbol;
        this.freq = freq;
    }
}
