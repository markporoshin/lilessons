package lesson4;

import java.io.*;
import java.util.*;

public class lesson4 {

    public static void code(String codeFile, String textFile, String outputFile) {
        Map<String, String> codes = new HashMap<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(codeFile))) {
            String line;
            while ((line = reader.readLine()) != null) {
                for (int i = 0; i < line.length(); i++) {
                    String[] tokens = line.split(" ");
                    codes.put(tokens[0], tokens[1]);
                }
            }
        } catch (IOException e) {

        }

        try (
                BufferedReader textReader = new BufferedReader(new FileReader(textFile));
                BufferedWriter encoderWriter = new BufferedWriter(new FileWriter(outputFile))
        ) {
            String line;
            while ((line = textReader.readLine()) != null) {
                for (int i = 0; i < line.length(); i++) {
                    char curSym = line.charAt(i);
                    String encodedChar = "";
                    if (codes.containsKey("" + curSym)) {
                        encodedChar = codes.get("" + curSym);
                    } else {
                        encodedChar = "" + curSym;
                    }
                    encoderWriter.write(encodedChar);
                }
                encoderWriter.newLine();
            }
        } catch (IOException e) {

        }
    }

    public static Map<Character, Float> countFreq(String fileName) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line;
            List<Character> symbol = new ArrayList<>();
            while ((line = reader.readLine()) != null) {
                for (int i = 0; i < line.length(); i++) {
                    symbol.add(line.charAt(i));
                }
            }
            Set<Character> unique = new HashSet<>(symbol);
            Map<Character, Float> map =new HashMap<Character, Float>();
            for (Character sym: unique) {
                map.put(sym, 0.f);
            }
            for (Character sym1: symbol) {
                float count = map.get(sym1);
                count++;
                map.put(sym1, count);
            }
            int size = symbol.size();
            for (Character sym: map.keySet()) {
                map.put(sym, map.get(sym) / size);
            }
            return map;
        } catch (IOException e) {
            return null;
        }
    }

    public static void main(String[] args) {
        code("code.txt", "encoded_text.txt", "decoded_text.txt");
//        Map<Character, Float> map = countFreq("encoded_text.txt");
//        for (Character sym : map.keySet()) {
//            System.out.println(sym + " " + map.get(sym));
//        }
        List<Pair> freq = new ArrayList<Pair>();
        freq.add(new Pair("a", 0.7f));
        freq.add(new Pair("c", 0.1f));
        freq.add(new Pair("b", 0.2f));

        freq.sort((a, b) ->  {
            if(a.freq > b.freq)
                return 1;
            else if(a.freq == b.freq) {
                return 0;
            }
            return -1;
        });

        System.out.println(freq);
    }

}
