package lesson3;

import java.io.*;
import java.util.*;

public class Lesson3 {

    static void printBin(int a) {
        if (a == 0) {
            return;
        }
        int r = a % 2;
        printBin(a / 2);
        System.out.print(r);
    }

    static int pow(int a, int n) {
        if (n == 0) {
            return 1;
        }
        return pow(a, n-1) * a;
    }

    static int f(int n){
        if(n == 0){
            return 1;
        }
        return f(n-1)*(n);
    }

    public static void main(String[] args) throws IOException {
//        BufferedWriter writer = new BufferedWriter(new FileWriter("text.txt"));
//
//        int n = 9;
//        char t = 'r';
//
//        double f = 8.9;
//
//        writer.write(String.format("Hello, this number is %d, char t is %c, %.2f\n", n, t, f));
//        writer.write("world\n");
//        writer.write("asd,jabs\n");
//
//        writer.close();

//        try(BufferedReader reader = new BufferedReader(new FileReader("text.txt"))) {
//            String line;
//            while ((line = reader.readLine()) != null) {
//                String[] words = line.split(" ");
//            }
//        }
        int lines = 212181 / 2;
        BufferedReader reader = new BufferedReader(new FileReader("The Lord of the Rings.txt"));
        String line;
        List<String> words = new ArrayList<>();
        int i = 0;
        while ((line=reader.readLine())!=null && i++ < lines / 2) {
            words.addAll(Arrays.asList(line.split("\\W+")));
        }
        Set<String> unique = new TreeSet<>(words);
        System.out.println(unique);
        Map<String, Integer> wordToCount = new HashMap<>();
        for (String word : unique) {
            wordToCount.put(word, 0);
        }
        for (String word: wordToCount.keySet()) {
            for (int j = 0; j < words.size(); j++) {
                if (word.equals(words.get(j))) {
                    int countOfWord = wordToCount.get(word);
                    wordToCount.put(word, countOfWord + 1);
                }
            }

        }
        for (String word: wordToCount.keySet()) {
            System.out.println(word + " " + wordToCount.get(word));
        }

        BufferedWriter writer=new BufferedWriter(new FileWriter("wordToCount2.txt"));
        for (String word: wordToCount.keySet()) {
            writer.write(""+ word  +" "+ wordToCount.get(word)+ "\n");

        }
        writer.close();
//        wordToCount.put("a", 3);
//        System.out.println(wordToCount.get("a"));

    }


}
