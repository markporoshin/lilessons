package lesson1;

public class MyInt {
    private int a;

    public MyInt(int a) {
        this.a = a;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    @Override
    public String toString() {
        return "" + a;
    }
}
