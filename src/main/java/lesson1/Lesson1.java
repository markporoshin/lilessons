package lesson1;

import java.util.Random;

public class Lesson1 {
    public static void main(String[] args) {
        Random random = new Random();
        int[] a = new int[] {
            65, 59, 66, 61, 53, 58, 50, 51, 60, 66
        };
//        for (int i = 0; i < a.length; i++) {
//            int b = Math.abs(random.nextInt()) % 20 + 50;
//            a[i] = b;
//            System.out.print(a[i] + " ");
//        }
        System.out.println();

        int max = 0;
        for (int i = 0; i < a.length; i++) {
            if (max < a[i]) {
                max = a[i];

            }
        }
        System.out.println(max);

        int max1 = 0;
        int max2 = 0;
        for (int i = 0; i < a.length; i++) {
            if (max1 < a[i]) {
                max1 = a[i];
            } else if (max2 < a[i] && a[i] != max1) {
                max2 = a[i];

            }
        }
        System.out.println(max1 + " " + max2);

        MyInt b = new MyInt(0);
        foo(b);
        System.out.println("b = " + b);
    }

    static void foo(MyInt a) {
        a.setA(a.getA() + 1);
    }
}
