package lesson2;

public class Lesson2 {

    public static void main(String[] args) {
        int n=3;
        task2(n);
    }


    public static void task1(int n) {
        String [][]mat = new String[n][n];
        for (int i = 0; i < mat.length; i++) {
            if(i==mat.length-1||i==0){
                for (int j = 0; j < mat.length ; j++) {
                    mat[i][j]="S";
                }

            }
            else{
                mat[i][0]="S";
                mat[i][mat.length-1]="S";
            }


        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (mat[i][j] == null) {
                    System.out.print(" ");
                } else {
                    System.out.print(mat[i][j]);
                }
            }
            System.out.println("");
        }
    }

    public static void task2(int n) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if(i == 0 || i == n-1){
                    System.out.print("S");
                } else if(j == 0 || j==n-1) {
                    System.out.print("S");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println("");
        }
    }

    public static void task3(int n) {

    }
}
